<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');
Route::post('resendverif', 'Api\UserController@resendverif');
Route::post('create', 'Api\PasswordResetController@create');
Route::get('find/{token}', 'Api\PasswordResetController@find');
Route::post('reset', 'Api\PasswordResetController@reset');
Route::get('gcs','Api\CategoryController@create');
Route::get('email/verify/{id}', 'Api\VerificationApiController@verify')->name('verificationapi.verify');
Route::get('email/resend', 'Api\VerificationApiController@resend')->name('verificationapi.resend');
Route::get('visitor','Api\VisitorController@index');
Route::get('ads/counter/{pointer}', 'Api\AdsController@viewCounter');
Route::get('event/counter/{pointer}', 'Api\EventController@viewCounter');
Route::get('book/counter/{pointer}', 'Api\BookController@addViewCount');
Route::get('zzcoba', 'Api\EventController@getAll');

Route::group(['middleware' => 'auth:api'], function() {

    Route::get('detail', 'Api\UserController@details')->middleware('verified');;
    Route::post('updatePhoto/{id}', 'Api\UserController@updatePhoto');
    Route::post('changePassword/{id}', 'Api\UserController@changePassword');

    Route::get('category','Api\CategoryController@getAll');
    Route::get('category/book/{id}', 'Api\CategoryController@getBookByCategory');

    Route::get('ads','Api\AdsController@getAll');
    Route::get('ads/{pointer}', 'Api\AdsController@show');
    


    Route::get('member', 'Api\MemberController@show');

    Route::get('books', 'Api\BookController@getAll');
    Route::get('book/{pointer}', 'Api\BookController@show');
    Route::get('book/search/{name}', 'Api\BookController@searchBook');

    Route::get('events', 'Api\EventController@getAll');
    Route::get('home', 'Api\EventController@getHomeData');
    Route::get('event/{pointer}', 'Api\EventController@show');

    Route::post('subscribe/store', 'Api\SubscribeController@store');

    Route::post('donation/store', 'Api\DonationController@store');

});
