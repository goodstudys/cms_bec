<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('login');
Route::post('reset', 'Api\PasswordResetController@reset')->name('reset.password.api');
Auth::routes(['verify' => true]);

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth' => 'dashboard']], function () {

    Route::get('/', 'DashboardController@index')->name('home');

    Route::get('/subscriber', 'SubscribeController@index')->name('subscribe.index');
    Route::get('/subscriber/show/{pointer}', 'SubscribeController@show')->name('subscribe.show');

    Route::get('/member', 'MemberController@index')->name('member.index');
    Route::get('/member/show/{pointer}', 'MemberController@show')->name('member.show');

    Route::get('/contributor', 'DonationController@index')->name('contributor.index');
    Route::get('/contributor/show/{pointer}', 'DonationController@show')->name('contributor.show');

    Route::resource('event', 'EventController');

    Route::resource('book', 'BookController');

    Route::resource('ads', 'AdsController');

    Route::resource('categories', 'CategoriesController');
    Route::get('/googlecloud', 'CategoriesController@googlecloud')->name('categories.googlecloud');
    Route::post('/googlecloud', 'CategoriesController@googlecloudUpload')->name('categories.googlecloudUpload');
    Route::get('/googlecloud/op', 'CategoriesController@googlecloudOpen')->name('categories.googlecloudOpen');

});

