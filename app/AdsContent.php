<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsContent extends Model
{
    protected $fillable = [
        'title',
        'link',
        'street',
        'description_short',
        'description_full',
        'image' ,
        'view_content',
        'date_start',
        'date_end' ,
        'row_pointer',
        'created_by',
        'created_at' 
    ];

    //
}
