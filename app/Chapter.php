<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $fillable = [
        'bood_id',
        'line',
        'title',
        'chapter_content',
        'row_pointer'
    ];
}
