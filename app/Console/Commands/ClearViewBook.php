<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearViewBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ClearViewBook';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto clear view book every 1 month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        // foreach ($data as $book) {
        //     $book->update(['view_count' => 0]);
        // }
        DB::table('books')
        ->update([
            'view_count' => 0
        ]);

        echo "Auto clear\n";
    }
}
