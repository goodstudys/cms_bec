<?php

namespace App\Http\Controllers;

use App\Chapter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'chapter_content' => 'required|mimes:pdf|max:10000'
        ]);

        $line = Chapter::where('book_id', $request->book)->max('line') + 1;

        $chapterName = time() . '.' . request()->chapter_content->getClientOriginalExtension();

        request()->chapter_content->move(public_path() . '/upload/book/chapter/files/', $chapterName);

        Chapter::insert([
            'book_id' => $request->book,
            'line' => $line,
            'title' => $request->title,
            'chapter_content' => $chapterName,
            'row_pointer' => Uuid::uuid4()->getHex(),
            'created_at' => Carbon::now()
        ]);

        return redirect()->route('book.index')->with('success','Chapter created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function show(Chapter $chapter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function edit(Chapter $chapter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chapter $chapter)
    {
        $request->validate([
            'title' => 'required',
            'chapter_content' => 'required|mimes:pdf|max:10000'
        ]);

        if ($request->chapter_content != null) {

            $chapterName = time() . '.' . request()->chapter_content->getClientOriginalExtension();

            request()->chapter_content->move(public_path() . '/upload/book/chapter/files/', $chapterName);

        } else {

            $chapterName = $chapter->chapter_content;

        }

        Chapter::insert([
            'title' => $request->title,
            'chapter_content' => $chapterName,
            'updated_at' => Carbon::now()
        ]);

        return redirect()->route('book.index')->with('success','Chapter updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chapter $chapter)
    {
        $chapter->delete();

        return redirect()->route('book.index')->with('success','Chapter deleted successfully.');
    }
}
