<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Ramsey\Uuid\Uuid;

class UserController extends Controller
{
    use VerifiesEmails;
    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    // public function login()
    // {
    //     if (\Illuminate\Support\Facades\Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
    //         $user = Auth::user();
    //         $success = $user->createToken('MyApp')->accessToken;
    //         return response()->json([
    //             'status' => 'success',
    //             'token_type' => 'Bearer',
    //             'token' => $success,
    //         ], $this->successStatus);
    //     } else {
    //         return response()->json(['error' => 'Unautorized'], 401);
    //     }
    // }
    // /**
    //  * Register api
    //  *
    //  * @return \Illuminate\Http\Response
    //  */

//     public function ValidateRegister(Request $req)
    // {
    //     $validatedData = $req->validate([
    //         'email' => 'required|email',
    //         'name' => 'required',
    //         'password' => 'required|min:6',
    //         'c_password' => 'required|same:password'
    //     ]);

//     return response()->json([
    //                 'status' => 'error',
    //                 'message' => 'Photo failed to update' ,
    //             ]);

// }
    //     public function abc(Request $request)
    //     {
    //         $validator = Validator::make($request->all(), [
    //             'name' => 'required',
    //             'email' => 'required|email',
    //             'password' => 'required',
    //             'c_password' => 'required|same:password',
    //         ]);
    //         if ($validator->fails()) {
    //             return response()->json(['error' => $validator->errors()], 401);
    //         }
    //         $input = $request->all();
    //         $id = User::max('id') + 1;
    //         $input['password'] = bcrypt($input['password']);
    //         $input['id'] = $id;
    //         $input['level'] = '3';
    //         $input['row_pointer'] = Uuid::uuid4()->getHex();
    //         $input['dateCreated'] = Carbon::now();
    //         $user = User::create($input);
    //         $success['token'] = $user->createToken('MyApp')->accessToken;
    //         $success['name'] = $user->name;
    //             return response()->json([
    //                 'status' => 'error',
    //                 'message' => 'Photo failed to update' ,
    //             ]);
    //         // return response()->json(['status' => 'success', 'success' => $success], $this->successStatus);
    //     }
    // /**
    //  * details api
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function details()
    // {
    //     $user = Auth::user();
    //     return response()->json(['success' => $user], $this->successStatus);
    // }

    // edit photo
    public function updatePhoto(Request $request, $id)
    {

        DB::beginTransaction();
        try {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(public_path() . '/upload/user/', $imageName);

            $imageUrl = url('/upload/user/' . $imageName);

            $data = User::where('row_Pointer', $id);

            $data->update([
                'photo' => $imageName,
            ]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Photo saved successfully',
                'url' => $imageUrl,
                'imageName' => $imageName,
            ], 200);

        } catch (Exception $e) {

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => 'Photo failed to update' . $e,
            ]);
        }
    }
    public function changePassword(Request $request, $id)
    {

        DB::beginTransaction();

        try {

            $data = User::where('id', $id);

            $check = User::where('id', $id)->first();

            if (!Hash::check($request->cur_pass, $check->password)) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Password lama anda salah',
                ]);
            } else {
                $data->update([
                    'password' => Hash::make($request->password),
                ]);

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Password berhasil dirubah',
                ]);
            }

        } catch (Exception $e) {

            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Password failed to save' . $e,
            ]);
        }
    }
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            if ($user->email_verified_at !== null) {
                $success = $user->createToken('MyApp')->accessToken;
                return response()->json([
                    'status' => 'success',
                    'token_type' => 'Bearer',
                    'token' => $success,
                ], $this->successStatus);
                $success['message'] = 'Login successfull';
                return response()->json(['success' => $success], $this->successStatus);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Please Verify Email'], 401);
            }
        } else {
            return response()->json(['status' => 'error', 'message' => 'Incorrect email or password'], 402);
        }
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $id = DB::table('users')->max('id') + 1;
        $user = DB::table('users')->insert([
            'id' => $id,
            'name' => $request->name,
            'level' => 3,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'row_pointer' =>  Uuid::uuid4()->getHex(), 
            'created_at' => Carbon::now(),
        ]);
        UserController::kirim($request->email, $request->password);
        return response()->json(['status' => 'success'], 200);
        // $input['password'] = Hash::make($input['password']);
        // $input['level'] = '3';
        // $input['row_pointer'] = Uuid::uuid4()->getHex();
        // $input['dateCreated'] = Carbon::now();
        // $input = $request->all();
        // $user = User::create($input);
        // $user->sendApiEmailVerificationNotification();
        // $success['message'] = 'Please confirm yourself by clicking on verify user button sent to you on your email';
        // return response()->json(['success' => $success], $this->successStatus);
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
    public function kirim($email, $password)
    {
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = Auth::user();
            $user->sendApiEmailVerificationNotification();
            return response()->json(['status' => 'success', 'message' => 'Please confirm yourself by clicking on verify user button sent to you on your email'], $this->successStatus);
            // } else {
            //     return response()->json(['status' => 'error', 'message' => 'Please Verify Email'], 401);
            // }
        } else {
            return response()->json(['status' => 'error', 'message' => 'Unauthorised'], 401);
        }
    }
    public function resendverif()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $user->sendApiEmailVerificationNotification();
            return response()->json(['status' => 'success', 'message' => 'Please confirm yourself by clicking on verify user button sent to you on your email'], $this->successStatus);
            // } else {
            //     return response()->json(['status' => 'error', 'message' => 'Please Verify Email'], 401);
            // }
        } else {
            return response()->json(['status' => 'error', 'message' => 'Unauthorised'], 401);
        }
    }

}
