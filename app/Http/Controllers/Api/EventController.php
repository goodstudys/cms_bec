<?php

namespace App\Http\Controllers\Api;

use App\Event;
use App\Book;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    public function getAll()
    {
        //$data = Event::where('date_start','<=',Carbon::now())->where('date_end','>=',Carbon::now())->get();
$data = Event::get();
        if (!empty($data)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'events' => $data
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);

    }
    public function getHomeData()
    {
        $event = Event::all();
        $booknew = Book::orderBy('id','desc')->get();
        $book = Book::inRandomOrder()->limit(4)->get();
        $bookRandom = Book::inRandomOrder()->limit(4)->get();
        $bookPopular = Book::orderBy('view_count','desc')->take(4)->get();

        if (!empty($book)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'events' => $event,
                'booknew'=>$booknew,
                'books' => $book,
                'random'=>$bookRandom,
                'booksPopular' => $bookPopular
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);

    }

    public function show($pointer)
    {
        $data = Event::where('row_pointer', $pointer)->first();

        if (!empty($data)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'event' => $data
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);
    }
    public function viewCounter($pointer)
    {
        $data = Event::where('row_pointer', $pointer);
        $getData= $data->first();
        
        DB::beginTransaction();
        try {
            if ($getData == null) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'data Kosong',
                ], 200);
            } else {
                $jumlah = $getData->view_count + 1;
                $data->update([
                    'view_count' => $jumlah,
                ]);
    
                //Book::where('row_pointer', $pointer)->update(['view_count' => $data->view_count + 1]);
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' => 'jumlah klik = '.$jumlah,
                ], 200);
            }
            

        } catch (Exception $e) {

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => 'Event failed to update' . $e,
            ]);
        }
    }
}
