<?php

namespace App\Http\Controllers\Api;

use App\Book;
use App\Http\Controllers\Controller;
use DB;

class BookController extends Controller
{
    public function getAll()
    {
        $data = Book::all();

        if (!empty($data)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'data' => $data,
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);

    }
    public function show($pointer)
    {
        $data = Book::where('row_pointer', $pointer)->first();

        if (!empty($data)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'book' => $data,
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);
    }
    public function addViewCount($pointer)
    {
        $data = Book::where('row_pointer', $pointer);
        $getData= $data->first();
        
        DB::beginTransaction();
        try {
            if ($getData == null) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'data Kosong',
                ], 200);
            } else {
                $jumlah = $getData->view_count + 1;
                $data->update([
                    'view_count' => $jumlah,
                ]);
    
                //Book::where('row_pointer', $pointer)->update(['view_count' => $data->view_count + 1]);
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' => 'jumlah klik = '.$jumlah,
                ], 200);
            }
            

        } catch (Exception $e) {

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => 'Book failed to update' . $e,
            ]);
        }
    }
    public function searchBook($name)
    {
        try {
            $book = Book::where('title', 'like', "%{$name}%")->orWhere('author', 'like', "%{$name}%")->get();
            if (count($book) == 0) {
                $data = 0;
            } else {
                $data = $book;
            }
        
            return response()->json([
                'status' => 'success',
                'message' => 'successfully',
                'data' => $data,
            ], 200);
        } catch (Exception $e) {

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => 'book failed to search' . $e,
            ]);
        }

    }
}
