<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Subscribe;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class SubscribeController extends Controller
{
    public function store(Request $request)
    {
        $subscribe = new Subscribe;
        $subscribe->user_id = Auth::user()->id;
        $subscribe->month = $request->month;
        $subscribe->date = $request->date;
        $subscribe->year = $request->year;
        $subscribe->row_pointer = Uuid::uuid4()->getHex();
        $subscribe->created_by = Auth::user()->id;
        $subscribe->created_at = Carbon::now();

        $success = $subscribe->save();

        if (!$success) {
            return response()->json([
                'status' => 'error',
                'code' => 500,
                'message' => 'Error Saving',
            ], 500);
        } else {
            return response()->json([
                'status' => 'error',
                'code' => 200,
                'message' => 'Success save subscribe',
            ], 200);
        }
    }
}
