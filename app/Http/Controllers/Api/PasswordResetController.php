<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['status'=>'error',
                'message' => 'We can\'t find a user with that e-mail address.',
            ], 404);
        }

        $passwordReset = DB::table('password_resets')->updateOrInsert(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60),
            ]
        );
        // dd($passwordReset->first()->token);
        if ($user && $passwordReset) {
            $user->notify(
                new PasswordResetRequest(DB::table('password_resets')->where('email', $user->email)->first()->token)
            );
        }

        return response()->json(['status'=>'success',
            'message' => 'We have e-mailed your password reset link!',
        ]);
    }
    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = DB::table('password_resets')->where('token', $token)
            ->first();
            $passwordDelete = DB::table('password_resets')->where('token', $token);
        if (!$passwordReset) {
            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 404);
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordDelete->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 404);
        }
        // return response()->json($passwordReset);
        return view('auth.resetpasswordApi', ['token' => $passwordReset->token]);
    }
    /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'token' => 'required|string',
        ]);
        $passwordDelete = DB::table('password_resets')->where([
            ['token', $request->token],
            ['email', $request->email],
        ]);
        $passwordReset = DB::table('password_resets')->where([
            ['token', $request->token],
            ['email', $request->email],
        ])->first();
        if (!$passwordReset) {
            return response()->json([
                'message' => 'This password reset token is invalid.',
            ], 404);
        }

        $user = User::where('email', $passwordReset->email)->first();
        if (!$user) {
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address.',
            ], 404);
        }

        $user->password = bcrypt($request->password);
        $user->save();
        $passwordDelete->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        // return response()->json($user);
        // return view('auth.resetpasswordApi', ['token' => 'SUKSES']);
        return response()->json("Successful password reset!");
    }
}
