<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function show() {

        $data = User::where('id', Auth::user()->id)->first();

        if (!empty($data)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'member' => $data
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);

    }
}
