<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Book;
use Illuminate\Support\Facades\Storage;
use File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $data = DB::table('categories')->get();

        if (!empty($data)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'data' => $data,
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);
    }

    public function getBookByCategory($id)
    {
        $data = Book::where('category', $id)->get();
        if (count($data)!= 0) {
            $buku = $data;
        } else {
            $buku = 0;
        }
        

        if (!empty($data)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'data' => $buku,
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disk = Storage::disk('gcs');
        
        // check if a file exists
   
        // $disk->setVisibility('book/cover/cover1608001267.jpg', 'public');
        // $disk->setVisibility('book/pdf/pdf1608001269.pdf', 'public');
        $url = $disk->url('book/pdf/pdf1608001269.pdf');
        $exists = $disk->exists('book/pdf/pdf1608003829.pdf');
        // $av = Storage::disk('gcs')->publicUrl($url );
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => 'Success',
            'data' => $exists,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
