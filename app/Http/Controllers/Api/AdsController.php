<?php

namespace App\Http\Controllers\Api;

use App\AdsContent;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AdsController extends Controller
{
    public function getAll()
    {
        $data = AdsContent::where('date_start', '<=', Carbon::now())->where('date_end', '>=', Carbon::now())->get();

        if (!empty($data)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'events' => $data,
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);

    }
    public function show($pointer)
    {
        $data = AdsContent::where('row_pointer', $pointer)->first();

        if (!empty($data)) {
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'message' => 'Success',
                'event' => $data,
            ], 200);
        }
        return response()->json([
            'status' => 'error',
            'code' => 200,
            'message' => 'Record not found',
        ], 200);
    }
    public function viewCounter($pointer)
    {
        $data = AdsContent::where('row_pointer', $pointer);
        $getData= $data->first();
        
        DB::beginTransaction();
        try {
            if ($getData == null) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'data Kosong',
                ], 200);
            } else {
                $jumlah = $getData->view_count + 1;
                $data->update([
                    'view_count' => $jumlah,
                ]);
    
                //Book::where('row_pointer', $pointer)->update(['view_count' => $data->view_count + 1]);
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' => 'jumlah klik = '.$jumlah,
                ], 200);
            }
            

        } catch (Exception $e) {

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => 'Ads failed to update' . $e,
            ]);
        }
    }
}
