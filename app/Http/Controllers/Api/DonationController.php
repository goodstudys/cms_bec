<?php

namespace App\Http\Controllers\Api;

use App\Donation;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class DonationController extends Controller
{
    public function store(Request $request)
    {
        $donation = new Donation;
        $donation->user_id = Auth::user()->id;
        $donation->amount = $request->amount;
        $donation->note = $request->note;
        $donation->row_pointer = Uuid::uuid4()->getHex();
        $donation->created_at = Carbon::now();

        $success = $donation->save();

        if (!$success) {
            return response()->json([
                'status' => 'error',
                'code' => 500,
                'message' => 'Error Saving',
            ], 500);
        } else {
            return response()->json([
                'status' => 'error',
                'code' => 200,
                'message' => 'Success save donation',
            ], 200);
        }
    }
}
