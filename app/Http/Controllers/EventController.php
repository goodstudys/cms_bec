<?php

namespace App\Http\Controllers;

use App\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\File;
use Image;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Event::all();

        return view('event.index', ['events' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpeg,jpg,png|required|max:40000',
            'date_start' => 'required',
            'date_end' => 'required'
        ]);

        // $imageName = time() . '.' . request()->image->getClientOriginalExtension();

        // request()->image->move(public_path() . '/upload/events/', $imageName);
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        $img = Image::make($request->image->getRealPath());
        $img->resize(1080, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save(public_path() . '/upload/events/'. $imageName, 80);

        Event::insert([
            'title' => $request->title,
            'location' => $request->location,
            'street' => $request->street,
            'description' => $request->description,
            'image' => $imageName,
            'date_start' => $request->date_start,
            'date_end' => $request->date_end,
            'row_pointer' => Uuid::uuid4()->getHex(),
            'created_by' => Auth::user()->id,
            'created_at' => Carbon::now()
        ]);

        return redirect()->route('event.index')->with('success','Event created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('event.edit', ['event' => $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'location'=>'required',
            'street' => 'required',
        ]);
        if ($request->image != null) {
            $file_path1 = public_path("upload/events/" . $event->image);
            if (File::exists($file_path1)) {
                File::delete($file_path1);
            }
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            $img = Image::make($request->image->getRealPath());
            $img->resize(1080, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save(public_path() . '/upload/events/'. $imageName, 80);

        } else {

            $imageName = $event->image;

        }
        if($request->date_start == null){
            $mulai = $request->date_start_lama;
        }else{
            $mulai = $request->date_start;
        }
        if($request->date_end == null){
            $akhir = $request->date_end_lama;
        }else{
            $akhir = $request->date_end;
        }

        $event->update([
            'title' => $request->title,
            'description' => $request->description,
            'location' => $request->location,
            'street' => $request->street,
            'image' => $imageName,
            'date_start' => $mulai,
            'date_end' => $akhir,
            'updated_by' => Auth::user()->id,
            'updated_at' => Carbon::now()
        ]);

        return redirect()->route('event.index')->with('success','Event updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $file_path1 = public_path("upload/events/" . $event->image);

        if (File::exists($file_path1)) {
            File::delete($file_path1);
        }
        $event->delete();

        return redirect()->route('event.index')->with('success','Event deleted successfully.');
    }
}
