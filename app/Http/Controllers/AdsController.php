<?php

namespace App\Http\Controllers;

use App\AdsContent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Image;
use Ramsey\Uuid\Uuid;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('ads_contents')->get();

        return view('ads.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'title' => 'required',
        //     'description' => 'required',
        //     'image' => 'mimes:jpeg,jpg,png|required|max:40000',
        //     'date_start' => 'required',
        //     'date_end' => 'required'
        // ]);

        ///resize//
        $imageName = time() . '.' . request()->image->getClientOriginalExtension();
        $img = Image::make($request->image->getRealPath());
        $img->resize(null, 1920, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save(public_path() . '/upload/ads/' . $imageName, 80);
        // ->colorize(0, 30, 0)
        ///resize//

        // request()->image->move(public_path() . '/upload/ads/', $imageName);

        DB::table('ads_contents')->insert([
            'title' => $request->title,
            'link' => $request->link,
            'street' => $request->street,
            'description_short' => $request->description_short,
            'description_full' => $request->description_full,
            'image' => $imageName,
            'date_start' => $request->date_start,
            'date_end' => $request->date_end,
            'row_pointer' => Uuid::uuid4()->getHex(),
            'created_by' => Auth::user()->id,
            'created_at' => Carbon::now(),
        ]);

        return redirect()->route('ads.index')->with('success', 'Iklan Baris created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(AdsContent $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit($event)
    {
        $data = DB::table('ads_contents')->where('id', $event)->first();
        return view('ads.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = DB::table('ads_contents')->where('id', $id);
        $isi = $data->first();
        if ($request->image != null) {
            $file_path1 = public_path("upload/ads/" . $isi->image);
            if (File::exists($file_path1)) {
                File::delete($file_path1);
            }
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            $img = Image::make($request->image->getRealPath());
            $img->save(public_path() . '/upload/ads/' . $imageName);

        } else {

            $imageName = $isi->image;

        }
        if ($request->date_start == null) {
            $mulai = $request->date_start_lama;
        } else {
            $mulai = $request->date_start;
        }
        if ($request->date_end == null) {
            $akhir = $request->date_end_lama;
        } else {
            $akhir = $request->date_end;
        }

        $data->update([
            'title' => $request->title,
            'link' => $request->link,
            'street' => $request->street,
            'description_short' => $request->description_short,
            'description_full' => $request->description_full,
            'image' => $imageName,
            'date_start' => $mulai,
            'date_end' => $akhir,
            'updated_by' => Auth::user()->id,
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('ads.index')->with('success', 'Iklan Baris updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy($event)
    {
        
        $data = DB::table('ads_contents')->where('id', $event);
        $dataAds = $data->first();
        $file_path1 = public_path("upload/ads/" . $dataAds->image);

        if (File::exists($file_path1)) {
            File::delete($file_path1);
        }
        $data->delete();

        return redirect()->route('ads.index')->with('success', 'Iklan Baris deleted successfully.');
    }
}
