<?php

namespace App\Http\Controllers;

use App\Book;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;
use Ramsey\Uuid\Uuid;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Book::all();

        return view('book.index', ['books' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = DB::table('categories')->get();
        return view('book.create', ['categories' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'thumbnail' => 'mimes:jpeg,jpg,png|required',
            'description' => 'required',
            'author' => 'required',
            'book' => 'required|mimes:pdf',
        ]);
        if ($request->thumbnail != null) {
            ///resize//
            $namagambar = time() . '.' . request()->thumbnail->getClientOriginalExtension();
            $img = Image::make($request->thumbnail->getRealPath());
            $img->resize(1080, 1920, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path() . '/upload/book/thumbnails/' . $namagambar, 80);
            ///resize//
            $fileCover = $request->thumbnail;
            $imageName = 'book/cover/cover' . time() . '.' . $fileCover->getClientOriginalExtension();
            Storage::disk('gcs')->put($imageName, File::get(public_path() . '/upload/book/thumbnails/' . $namagambar));
            Storage::disk('gcs')->setVisibility($imageName, 'public');
            // $urlcover = Storage::disk('gcs')->url($imageName);
            $file_path1 = public_path() . '/upload/book/thumbnails/' . $namagambar;
            if (File::exists($file_path1)) {
                File::delete($file_path1);
            }
        }
        if ($request->book != null) {
            $fileBook = $request->book;
            $bookName = 'book/pdf/pdf' . time() . '.' . $fileBook->getClientOriginalExtension();
            Storage::disk('gcs')->put($bookName, File::get($fileBook));
            Storage::disk('gcs')->setVisibility($bookName, 'public');
            // $urlpdf = Storage::disk('gcs')->url($bookName);
        }
        // request()->thumbnail->move(public_path() . '/upload/book/thumbnails/', $imageName);

        // $bookName = time() . '.' . request()->book->getClientOriginalExtension();

        // request()->book->move(public_path() . '/upload/book/files/', $bookName);

        Book::insert([
            'title' => $request->title,
            'thumbnail' => $imageName,
            'description' => $request->description,
            'category' => $request->category,
            'author' => $request->author,
            'book' => $bookName,
            'row_pointer' => Uuid::uuid4()->getHex(),
            'created_by' => Auth::user()->id,
            'created_at' => Carbon::now(),
        ]);

        return redirect()->route('book.index')->with('success', 'Book created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $data = DB::table('categories')->get();
        $urlcover = Storage::disk('gcs')->url($book->thumbnail);
        $urlbook = Storage::disk('gcs')->url($book->book);
        return view('book.edit', ['book' => $book, 'categories' => $data, 'urlcover' => $urlcover, 'urlbook' => $urlbook]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'author' => 'required',
        ]);
        $disk = Storage::disk('gcs');
        if ($request->thumbnail == null) {

            $imageName = $book->thumbnail;

        } else {
            $disk->delete($book->thumbnail);
            ///resize//
            $namagambar = time() . '.' . request()->thumbnail->getClientOriginalExtension();
            $img = Image::make($request->thumbnail->getRealPath());
            $img->resize(1080, 1920, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save(public_path() . '/upload/book/thumbnails/' . $namagambar, 80);
            ///resize//
            $fileCover = $request->thumbnail;
            $imageName = 'book/cover/cover' . time() . '.' . $fileCover->getClientOriginalExtension();
            $disk->put($imageName, File::get(public_path() . '/upload/book/thumbnails/' . $namagambar));
            $disk->setVisibility($imageName, 'public');
            $file_path1 = public_path() . '/upload/book/thumbnails/' . $namagambar;
            if (File::exists($file_path1)) {
                File::delete($file_path1);
            }
        }

        if ($request->bookk == null) {

            $bookName = $book->book;

        } else {
            $disk->delete($book->book);
            $fileBook = $request->bookk;
            $bookName = 'book/pdf/pdf' . time() . '.' . $fileBook->getClientOriginalExtension();
            $disk->put($bookName, File::get($fileBook));
            $disk->setVisibility($bookName, 'public');

        }

        $book->update([
            'title' => $request->title,
            'thumbnail' => $imageName,
            'description' => $request->description,
            'category' => $request->category,
            'author' => $request->author,
            'book' => $bookName,
            'updated_by' => Auth::user()->id,
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('book.index')->with('success', 'Book updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        Storage::disk('gcs')->delete($book->book);
        Storage::disk('gcs')->delete($book->thumbnail);
        return redirect()->route('book.index')->with('success', 'Book deleted successfully.');
    }
}
