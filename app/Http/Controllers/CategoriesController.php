<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;
use File;
use View;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('categories')->get();

        return view('categories.index', ['categories' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        DB::table('categories')->insert([
            'name' => $request->name,
            'row_pointer' => Uuid::uuid4()->getHex(),
            'created_by' => Auth::user()->id,
            'created_at' => Carbon::now(),
        ]);

        return redirect()->route('categories.index')->with('success', 'Category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        DB::table('categories')->where('id', $id)->update([
            'name' => $request->name,
            'updated_at' => Carbon::now(),
            'updated_by' => Auth::user()->id,
        ]);

        return redirect()->route('categories.index')->with('success', 'Category updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return redirect()->route('categories.index')->with('success', 'categories deleted successfully.');
    }
    public function googlecloud(){
        return view('googlecloud.index');
    }
    public function googlecloudUpload(Request $request){
        $files = $request->file('uploadFile');
        $filesLink = array();

        if ($files[0] != null) {
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();// Get the orginal filname
                Storage::disk('gcs')->put($filename, File::get($file));
                $url = Storage::disk('gcs')->url($filename);//get url after uploaded 
                array_push($filesLink, $url);//add url to $filesLink[]
            }
        }
        return redirect('googlecloud')->with('filesLink', $filesLink);
    }
    public function googlecloudOpen(){
        $contents = Storage::disk('gcs')->get('logo_bec.png');
        Storage::disk('gcs')->setVisibility('logo_bec.png', 'public');
        return view('googlecloud.index')->with('data', $contents);
    }
}
