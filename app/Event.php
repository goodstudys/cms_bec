<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'description',
        'street',
        'location',
        'image',
        'date_start',
        'date_end',
        'created_by',
        'updated_by'
    ];
}
