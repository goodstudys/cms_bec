<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'title',
        'thumbnail',
        'description',
        'category',
        'author',
        'book',
        'row_pointer',
        'created_by',
        'updated_by'
    ];
}
