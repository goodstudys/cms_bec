<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'level' => 1,
            'phone' => '08123456789',
            'email' => 'admin@goodnews.id',
            'password' => Hash::make('admin'),
            'row_pointer' => Uuid::uuid4()->getHex()
        ]);
    }
}
