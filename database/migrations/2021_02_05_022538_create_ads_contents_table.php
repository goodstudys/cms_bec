<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_contents', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description_short');
            $table->text('description_full');
            $table->string('image')->nullable();
            $table->string('link');
            $table->string('street');
            $table->bigInteger('view_count')->default(0);
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->string('row_pointer');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_contents');
    }
}
