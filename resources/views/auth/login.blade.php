@extends('layouts.app')

@section('content')
<!-- Icon -->
<div class="fadeIn first">
    <img src="{{asset('img/logo_bec.jpg')}}" style="width: 30%" id="icon" alt="User Icon" />
</div>

<!-- Login Form -->
<form method="POST" action="{{ route('login') }}"  enctype="multipart/form-data">
    @csrf
    <input id="email" type="email" class="fadeIn second @error('email') is-invalid @enderror" value="{{ old('email') }}"
        name="email" placeholder="email" required autocomplete="email" autofocus>
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    <input type="password" id="password" class="fadeIn third @error('password') is-invalid @enderror" name="password"
        placeholder="password" required autocomplete="current-password">
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    <input type="submit" class="fadeIn fourth" value="Log In">
</form>

<!-- Remind Passowrd -->
<div id="formFooter">
    <a class="underlineHover" href="http://mygoodnews.id/">Copyright &copy; {{ Carbon\Carbon::now()->format('Y')}} Develope By MyGoodnews</a>
</div>
{{-- <div class="container">
    
    <div class="row justify-content-center">
        <div class="login">
            <div class="login-triangle"></div>
            
            <h2 class="login-header">Log in</h2>
          
            <form class="login-container">
              <p><input type="email" placeholder="Email"></p>
              <p><input type="password" placeholder="Password"></p>
              <p><input type="submit" value="Log in"></p>
            </form>
          </div> --}}
{{-- <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

<div class="card-body">
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                        {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif
            </div>
        </div>
    </form>
</div>
</div>
</div> --}}
{{-- </div>
</div> --}}
@endsection