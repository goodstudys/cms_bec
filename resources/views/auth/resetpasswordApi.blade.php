@extends('layouts.app')

@section('content')
<!-- Icon -->
@if ($token == 'SUKSES')
<div class="fadeIn first">
    <img src="{{asset('img/logo_bec.jpg')}}" style="width: 30%" id="icon" alt="User Icon" />
<br>
<br>
<h1>Successful password reset</h1>
</div>

@else
<div class="fadeIn first">
    {{-- <img src="{{asset('img/logo_bec.jpg')}}" style="width: 30%" id="icon" alt="User Icon" /> --}}
<h1>Reset Password</h1>
</div>

<!-- Login Form -->
<form method="POST" action="{{ route('reset.password.api') }}"  enctype="multipart/form-data">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @csrf
    <input id="email" type="email" class="fadeIn second" value="{{ old('email') }}"
        name="email" placeholder="email" required autocomplete="email" autofocus>
    <input type="password" id="txtNewPassword" class="fadeIn third" name="password"
        placeholder="new password" required autocomplete="current-password" required>
        <input type="password" class="fadeIn third" id="txtConfirmPassword" placeholder="Confirm Passward" name="confpass" required>
        <div class="registrationFormAlert" style="color:red;" id="CheckPasswordMatch"></div>
        <input type="text" style="display: none" name="cekvalue" id="cekvalue" required>
        <input type="text" id="password" class="fadeIn third" name="token"  
        style="display: none"
        placeholder="Token" value="{{$token}}" required autocomplete="current-password">
    <input type="submit" class="fadeIn fourth" value="Reset">
</form>
@endif


<!-- Remind Passowrd -->
<div id="formFooter">
    <a class="underlineHover" href="http://mygoodnews.id/">Copyright &copy; {{ Carbon\Carbon::now()->format('Y')}} Develope By MyGoodnews</a>
</div>
@endsection
@section('extrascript')
<script>
    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();
        if (password != confirmPassword){
            $("#CheckPasswordMatch").html("Passwords does not match!");
            $("#cekvalue").val("");
        }else{
            $("#CheckPasswordMatch").html("");
            $("#cekvalue").val("b");
        }
    }
    $(document).ready(function () {
       $("#txtConfirmPassword").keyup(checkPasswordMatch);
    });
    </script>
@endsection