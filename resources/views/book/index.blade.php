@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Book</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="{{ route('book.index') }}">Book</a></div>
                    {{--                    <div class="breadcrumb-item">DataTables</div>--}}
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="section-body">
                <h2 class="section-title">Book
                    <div class="float-right">
                        <a href="{{ route('book.create') }}" class="btn btn-primary">Create Book</a>
                    </div>
                </h2>
                <p class="section-lead">
                    List all book
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Book</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Title</th>
                                            <th>Click</th>
                                            <th>Thumbnail</th>
                                            <th width="20%">Description</th>
                                            <th>author</th>
                                            <th width="15%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($books as $index => $book)
                                            <tr>
                                                <td class="text-center">{{ $index+1 }}</td>
                                                <td>{{ $book->title }}</td>
                                                <td>{{$book->view_count}}</td>
                                                <td><img id="img" src="{{'https://storage.googleapis.com/bec_collection/'.$book->thumbnail }}" alt="" style="width: 100%; max-width: 100px; height: auto;"></td>
                                                <td>{{ \Illuminate\Support\Str::limit(strip_tags($book->description), '100', ' ...') }}</td>
                                                <td>{{ $book->author }}</td>
                                                <td>
                                                    <form action="{{ route('book.destroy', $book->id) }}" method="post">
                                                        <a href="{{ route('book.edit', $book->id) }}" class="btn btn-warning">Edit</a>

                                                        @csrf
                                                        @method('DELETE')

                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
