@extends('layouts.dashboard')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="#" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Edit Iklan Baris</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="{{ route('ads.index') }}">Iklan Baris</a></div>
                <div class="breadcrumb-item">Edit Iklan Baris</div>
            </div>
        </div>

        <div class="section-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <h2 class="section-title">Edit Iklan Baris</h2>
            <p class="section-lead">
                On this page you can update a Iklan Baris and fill in all fields.
            </p>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Edit Your Iklan Baris</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('ads.update', $data->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control" name="title" value="{{ $data->title }}">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Link</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control" name="link"
                                            value="{{ $data->link }}" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Street</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control" name="street"
                                            value="{{ $data->street }}" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Description
                                        Short</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea name="description_short" maxlength="50" id="" cols="30" rows="10"
                                            class="form-control">{{ $data->description_short }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Description
                                        Full</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea name="description_full" id="" cols="30" rows="10"
                                            class="form-control">{{ $data->description_full }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image
                                        Event</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div id="image-preview" class="image-preview"
                                            style="width: 500px; background-image: url('{{ asset('upload/ads/'. $data->image) }}'); background-size: 100% 100%;">
                                            <label for="image-upload" id="image-label">Choose File</label>
                                            <input type="file" name="image" id="image-upload"
                                                accept="image/gif, image/jpeg, image/png,image/jpg" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Date
                                        Start</label>

                                    <div class="col-sm-12 col-md-7">
                                        {{ $data->date_start }}
                                        <input type="datetime-local" class="form-control" name="date_start"
                                            value="{{ $data->date_start }}">
                                        <input style="display: none" type="text" class="form-control"
                                            name="date_start_lama" value="{{ $data->date_start }}">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Date
                                        End</label>
                                    <div class="col-sm-12 col-md-7">
                                        {{ $data->date_end }}
                                        <input type="datetime-local" class="form-control" name="date_end"
                                            value="{{ $data->date_end }}">
                                        <input style="display: none" type="text" class="form-control"
                                            name="date_end_lama" value="{{ $data->date_end }}">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button class="btn btn-primary">Edit Iklan Baris</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop