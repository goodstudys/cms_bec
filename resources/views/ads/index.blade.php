@extends('layouts.dashboard')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Iklan Baris</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="{{ route('ads.index') }}">Iklan Baris</a></div>
                {{--                    <div class="breadcrumb-item">DataTables</div>--}}
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="section-body">
            <h2 class="section-title">Iklan Baris
                <div class="float-right">
                    <a href="{{ route('ads.create') }}" class="btn btn-primary">Create Iklan Baris</a>
                </div>
            </h2>
            <p class="section-lead">
                List all Iklan Baris
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Iklan Baris</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Title</th>
                                            <th>Click</th>
                                            <th>Image</th>
                                            <th width="20%">Short Description</th>
                                            <th>link</th>
                                            <th width="15%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $index => $item)
                                        <tr>
                                            <td class="text-center">{{ $index+1 }}</td>
                                            <td>{{ $item->title }}</td>
                                            <td>{{$item->view_count}}</td>
                                            <td><img id="img" src="{{asset('/upload/ads/'.$item->image) }}" alt=""
                                                    style="width: 100%; max-width: 100px; height: auto;"></td>
                                            <td>{{ \Illuminate\Support\Str::limit(strip_tags($item->description_short), '100', ' ...') }}
                                            </td>
                                            <td>{{ $item->link }}</td>
                                            <td>
                                                <form action="{{ route('ads.destroy', $item->id) }}" method="post">
                                                    <a href="{{ route('ads.edit', $item->id) }}"
                                                        class="btn btn-warning">Edit</a>

                                                    @csrf
                                                    @method('DELETE')

                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop