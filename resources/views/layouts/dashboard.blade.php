<!DOCTYPE html>
<html lang="en">

@include('partials._head')

<body>
<div id="app">
    <div class="main-wrapper">

        @include('partials._navbar')

        @include('partials._sidebar')

        <!-- Main Content -->
        @yield('content')

        @include('partials._footer')
    </div>
</div>

@include('partials._script')
</body>
</html>
