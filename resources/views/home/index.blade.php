@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card card-statistic-2">
                        <div class="card-stats">
                            <div class="col text-center mt-3 mb-3 me-3">
                                <h4>Total Buku</h4>
                                <h3>{{$buku}}</h3>
                            </div>
                            {{-- <canvas id="balance-chart" height="80"></canvas> --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card card-statistic-2">
                        <div class="card-chart">
                            <div class="col text-center mb-4 me-3">
                                <h4>Total Member</h4>
                                <h3>{{$member}}</h3>
                            </div>
                            {{-- <canvas id="balance-chart" height="80"></canvas> --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card card-statistic-2">
                        <div class="card-chart">
                            <div class="col text-center mb-4 me-3">
                                <h4>Total Iklan</h4>
                                <h3>{{$iklan}}</h3>
                            </div>
                            {{-- <canvas id="sales-chart" height="80"></canvas> --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card card-statistic-2">
                        <div class="card-stats">
                            <div class="col text-center mt-3 mb-3 me-3">
                                <h4>Total Visitor Hari Ini</h4>
                                <h3>{{$visitor}}</h3>
                            </div>
                            {{-- <canvas id="balance-chart" height="80"></canvas> --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
