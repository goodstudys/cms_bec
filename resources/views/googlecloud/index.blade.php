@extends('layouts.dashboard')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Event</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="{{ route('event.index') }}">Event</a></div>
                {{--                    <div class="breadcrumb-item">DataTables</div>--}}
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="section-body">
            <h2 class="section-title">Event
                <div class="float-right">
                    <a href="{{ route('event.create') }}" class="btn btn-primary">Create Event</a>
                </div>
            </h2>
            <p class="section-lead">
                List all event
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Event</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('categories.googlecloudUpload') }}" method="POST"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input name="uploadFile[]" type="file" multiple>
                                <button class="btn btn-default" type="submit">
                                    Submit
                                </button>
                                </input>
                            </form>
                            @if(session('filesLink')) {{-- check有無野 --}}
                            @foreach(session('filesLink') as $fileLink)
                            <a href="{{ $fileLink }}">{{ $fileLink }}</a>
                            <br />
                            @endforeach
                            @endif
                            @if(session('data')) {{-- check有無野 --}}
                            {{$data}}
                            @endif

                            {{-- <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Title</th>
                                            <th width="20%">Description</th>
                                            <th>Image</th>
                                            <th>Start</th>
                                            <th>End</th>
                                            <th width="15%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($events as $index => $event)
                                            <tr>
                                                <td class="text-center">{{ $index+1 }}</td>
                            <td>{{ $event->title }}</td>
                            <td>{{ \Illuminate\Support\Str::limit(strip_tags($event->description), '100', ' ...') }}
                            </td>
                            <td><img src="{{ asset('upload/events/'. $event->image) }}" alt=""
                                    style="width: 100%; max-width: 100px; height: auto;"></td>
                            <td>{{ \Carbon\Carbon::parse($event->date_start)->format('d-m-Y') }}</td>
                            <td>{{ \Carbon\Carbon::parse($event->date_end)->format('d-m-Y') }}</td>
                            <td>
                                <form action="{{ route('event.destroy', $event->id) }}" method="post">
                                    <a href="{{ route('event.edit', $event->id) }}" class="btn btn-warning">Edit</a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                            </tr>
                            @endforeach
                            </tbody>
                            </table>
                        </div> --}}
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <a href="{{ route('categories.googlecloudOpen') }}" class="btn btn-primary">Open</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>
@stop