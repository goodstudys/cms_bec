@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Subscriber</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="{{ route('subscribe.index') }}">Subscriber</a></div>
                    {{--                    <div class="breadcrumb-item">DataTables</div>--}}
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="section-body">
                <h2 class="section-title">Subscriber
                </h2>
                <p class="section-lead">
                    List all subscriber
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Subscriber</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th width="40%">Name</th>
                                            <th>Month</th>
                                            <th>Date</th>
                                            <th>Year</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($subscribes as $index => $subscribe)
                                            <tr>
                                                <td class="text-center">{{ $index+1 }}</td>
                                                <td>{{ $subscribe->name }}</td>
                                                <td>{{ $subscribe->month }}</td>
                                                <td>{{ $subscribe->date }}</td>
                                                <td>{{ $subscribe->year }}</td>
                                                <td>
                                                    <a href="{{ route('subscribe.show', $subscribe->row_pointer) }}" class="btn btn-info">Show</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
