<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('home') }}">Buddhist Education Centre</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('home') }}">LBEC</a>
        </div>
        <ul class="sidebar-menu">
            <li><a class="nav-link" href="{{ route('home') }}"><i class="fas fa-fire"></i><span>Dashboard</span></a></li>
            <li><a class="nav-link" href="{{ route('subscribe.index') }}"><i class="fas fa-ellipsis-h"></i><span>Subscriber</span></a></li>
            <li><a class="nav-link" href="{{ route('member.index') }}"><i class="fas fa-users"></i><span>Member</span></a></li>
            <li><a class="nav-link" href="{{ route('ads.index') }}"><i class="fas fa-file-alt"></i><span>Iklan Baris</span></a></li>
            <li><a class="nav-link" href="{{ route('book.index') }}"><i class="fas fa-book"></i><span>Book</span></a></li>
            <li><a class="nav-link" href="{{ route('categories.index') }}"><i class="fas fa-list-alt"></i><span>Categories</span></a></li>
            <li><a class="nav-link" href="{{ route('contributor.index') }}"><i class="fab fa-black-tie"></i><span>Contributor</span></a></li>
            <li><a class="nav-link" href="{{ route('event.index') }}"><i class="fas fa-columns"></i><span>Event</span></a></li>
            {{-- <li><a class="nav-link" href="{{ route('categories.googlecloud') }}"><i class="fas fa-columns"></i><span>Google Cloud</span></a></li> --}}
        </ul>
    </aside>
</div>
